# CHANGELOG



## v0.2.1 (2024-03-25)

### Fix

* fix(deploy): add deploy pipeline ([`0ef982f`](https://gitlab.com/BDomecq/my-package/-/commit/0ef982fef4d88b9b53f822e3f506d956daaf57a4))


## v0.2.0 (2024-03-25)

### Feature

* feat(addition): add addition feature ([`1b7d708`](https://gitlab.com/BDomecq/my-package/-/commit/1b7d708bf125229eef99c6a9e628de029e4957ec))


## v0.1.0 (2024-03-25)

### Feature

* feat(sr): add semantic release to the repository ([`73bf5c8`](https://gitlab.com/BDomecq/my-package/-/commit/73bf5c8015492d8d78b676253485a308a51c1eb8))

### Unknown

* add semantic_release config ([`32c3fd2`](https://gitlab.com/BDomecq/my-package/-/commit/32c3fd2c5e6397e658a2fb23ba23a674ae450326))

* add python-semantic-release dev dependency ([`8c82327`](https://gitlab.com/BDomecq/my-package/-/commit/8c82327b98584e9533dc1e0c3e18ac7ef2b4a74b))

* Add pyproject.toml to init poetry ([`662a735`](https://gitlab.com/BDomecq/my-package/-/commit/662a735a85e2afd428d4878425590e77701fe80b))

* Update .gitlab-ci.yml file ([`a8c77ce`](https://gitlab.com/BDomecq/my-package/-/commit/a8c77ce55921846b43f9b9def86fe7d9b1784b42))

* Add first pipeline ([`1b41cc5`](https://gitlab.com/BDomecq/my-package/-/commit/1b41cc55a421e6c01d050642b481fae21000621a))

* Initial commit ([`5e89752`](https://gitlab.com/BDomecq/my-package/-/commit/5e89752ac2ddc0ee35711575231ab32133a66fee))
